package com.bhadresh.matrimony.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;

import com.bhadresh.matrimony.AddUserActicity;
import com.bhadresh.matrimony.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {


    @BindView(R.id.cvActAddCandidate)
    CardView cvActAddCandidate;
    @BindView(R.id.cvActListCandidate)
    CardView cvActListCandidate;
    @BindView(R.id.cvActSearchCandidate)
    CardView cvActSearchCandidate;
    @BindView(R.id.cvActFavoriteCandidate)
    CardView cvActFavoriteCandidate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.dashboard), false);
    }

    @OnClick(R.id.cvActAddCandidate)
    public void onCvActAddCandidateClicked() {
        Intent intent = new Intent(this, AddUserActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.cvActListCandidate)
    public void onCvActListCandidateClicked() {
        Intent intent = new Intent(this, ActivityUserListByGender.class);
        startActivity(intent);
    }

    @OnClick(R.id.cvActSearchCandidate)
    public void onCvActSearchCandidateClicked() {
        Intent intent = new Intent();
        startActivity(intent);
    }

    @OnClick(R.id.cvActFavoriteCandidate)
    public void onCvActFavoriteCandidateClicked() {
        Intent intent = new Intent(this, FavoriteUserActivity.class);
        startActivity(intent);
    }
}