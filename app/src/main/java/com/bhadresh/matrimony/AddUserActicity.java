package com.bhadresh.matrimony;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddUserActicity extends AppCompatActivity {
    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.etFatherName)
    EditText etFatherName;
    @BindView(R.id.etSurName)
    EditText etSurName;
    @BindView(R.id.rbMale)
    RadioButton rbMale;
    @BindView(R.id.rbFeMale)
    RadioButton rbFeMale;
    @BindView(R.id.rgGender)
    RadioGroup rgGender;
    @BindView(R.id.etDob)
    EditText etDob;
    @BindView(R.id.etPhoneNumber)
    EditText etPhoneNumber;
    @BindView(R.id.etEmailAddress)
    EditText etEmailAddress;
    @BindView(R.id.spCity)
    Spinner spCity;
    @BindView(R.id.spLanguage)
    Spinner spLanguage;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.screen_Layout)
    FrameLayout screenLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.etDob)
    public void onEtDobClicked() {
    }

    @OnClick(R.id.btnSubmit)
    public void onBtnSubmitClicked() {
    }
}
